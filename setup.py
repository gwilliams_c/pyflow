from setuptools import setup

setup(
  name='pyflow',
  version='0.1',
  py_modules=['pyflow'],
  install_requires=[
    'Click',
    'click-repl'
    'python-gitlab'
  ],
  entry_points='''
    [console_scripts]
    pyflow=pyflow:cli
  ''',
)
