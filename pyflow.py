import gitlab
import click

class Pyflow:

    gl = None

    def __init__(self):
        self.gl = gitlab.Gitlab.from_config('psi_external', ['./gitlab.cfg'])

pflow = click.make_pass_decorator(Pyflow, ensure=True)

@click.group()
def cli():
    pass

@click.group('issues')
@click.pass_context
def issues():
    pass

@click.group('branch')
@click.pass_context
def branch():
    pass

@cli.command('issues')
@click.option('--group', default=None)
@click.option('--repo', default=None)
@pflow
def issues_group(pflow, group, repo):
    if(group and repo):
        click.echo("Cannot specify a group and a repository")
        return
    elif group:
        groups = pflow.gl.groups.search(group)
        if len(groups) == 1:
            # group_obj = p.gl.groups.get(groups[0].id)
            get_issues(groups[0].id, False)
        else:
            ind = get_choice_index('Found {} groups, you need to choose one:'.format(len(groups)), 'Please choose a group', groups, 'name')
            get_issues(groups[ind].id, False)
    elif repo:
        repos = pflow.gl.projects.list(search=repo)
        if len(repos) == 1:
            get_issues(pflow, repos[0].id)
        else:
            ind = get_choice_index('Found {} repos, you need to choose one:'.format(len(repos)), 'Please chooose a repo', repos, 'name')
            get_issues(pflow, repos[ind].id)
    elif not group and  not repo:
        click.echo('Group or repository must be specified')

def get_issues(pflow, id, repo=True):
    issues = pflow.gl.project_issues.list(project_id=id) if repo else pflow.gl.group_issues.list(group_id=id)
    ind = get_choice_index('Choose an issue', 'Please choose an issue', issues, 'title')

def get_choice_index(info, prompt, choices, prop):
    print(info)
    for key, choice in enumerate(choices):
        print("{} {}".format(key, getattr(choice, prop)))
    return click.prompt(prompt, type=int, default=0)

@cli.command('repo')
@click.option('--group', default='catamel')
def issues_group(group):
    p = Pyflow()
    groups = p.gl.groups.search(group)
    print(len(groups))
